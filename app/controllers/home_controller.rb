class HomeController < ApplicationController
  layout 'bootstrap'

  def index
    @page = Page.find_by_page_url('index') || demo_page
    setup_page(@page)
    render action: :page
  end

  def page
    redirect_to '/' and return if params[:page_url] == 'index'
    @page = Page.find_by_page_url params[:page_url]
    setup_page(@page)
  end

  def mercury_page_update
    @page = Page.find_by_page_url params[:page_url]
    @page.content = params[:content][:content][:value] rescue nil
    @page.save!
    render text: ""
  end

  protected
  def setup_page(page)
    @title = page.title
    @meta_description = @page.meta_description
    @content = page.content
    @editable = true if page.persisted?
  end

  def demo_page
    Page.new(title: "Demo Index Page", content: view_context.demo_content)
  end
end
