class ApplicationController < ActionController::Base
  protect_from_forgery
  helper_method :current_site

  def current_site
    @current_site ||= Site.find_by_domain(request.host.gsub('www.', '')) || Site.new(domain: 'example.com', header: 'Site Header', footer: 'Site Footer')
  end
end
