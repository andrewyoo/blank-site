class Site < ActiveRecord::Base
  attr_protected
  before_save :clean_domain

  validates_presence_of :domain
  
  private

  def clean_domain
    self.domain = self.domain.gsub('www.', '').strip
  end
end
