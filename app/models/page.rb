class Page < ActiveRecord::Base
  validates_presence_of :page_url
  validates_uniqueness_of :page_url
  attr_protected
end
