module HomeHelper
  def demo_content
    content = <<-content
Welcome to Blank Site

This page is auto generated until you make your own Page with page.page_url == 'index'

To access the backend, login at /admin with admin@example.com and password 'password'

Once you create a page and are logged in, you will see an edit link on the top right to edit the page inline.
content
    content_tag(:pre, content)
  end
end
