module ApplicationHelper
  def bootstrap_flash
    close_button = content_tag :button, '&times;'.html_safe, class: 'close', data: {dismiss: 'alert'}, aria: {hidden: 'true'}
    output = []
    flash.each do |k,v|
      case k
      when :notice
        output << content_tag(:div, close_button + v, class: 'alert alert-success alert-dismissable')
      when :info
        output << content_tag(:div, close_button + v, class: 'alert alert-info alert-dismissable')
      when :error
        output << content_tag(:div, close_button + v, class: 'alert alert-danger alert-dismissable')
      else
        output << content_tag(:div, close_button + v, class: 'alert alert-warning alert-dismissable')
      end
    end
    output.join.html_safe
  end
end
