class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :domain
      t.string :favicon_url
      t.text :css, limit: 16777216
      t.text :header, limit: 16777216
      t.text :footer, limit: 16777216
      t.timestamps
    end
  end
end
