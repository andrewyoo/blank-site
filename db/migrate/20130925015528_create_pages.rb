class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :page_url
      t.string :title
      t.text :meta_description
      t.text :meta_keywords
      t.text :content, limit: 16777216
      t.timestamps
    end
  end
end
