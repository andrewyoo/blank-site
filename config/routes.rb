BlankSite::Application.routes.draw do
  mount Mercury::Engine => '/'

  root to: 'home#index'
  ActiveAdmin.routes(self)
  devise_for :admin_users, ActiveAdmin::Devise.config

  match '/:page_url' => 'home#page'
  put   '/:page_url/mercury_page_update' => 'home#mercury_page_update', :as => :mercury_page_update
end
